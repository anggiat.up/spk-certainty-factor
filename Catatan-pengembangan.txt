

Database

	tbl_users
		- users_id
		- username
		- password
		- nama_lengkap
		- no_hp
		- jabatan
		- foto_profile
		- level_akun
			> ['operator','dokter','admin']
		- status_akun
			> ['aktif','tidak-aktif']
		- terakhir_login

	tbl_nilaicf
		- cf_id
		- cf_nilai
		- cf_ketentuan

	tbl_datapenyakit
		- kode_penyakit
		- nama_penyakit
		- gambar_penyakit
		- detail_penyakit
		- saran_pengobatan
		- saran_pencegahan

	tbl_gejalapenyakit
		- kode_gejala
		- gejala
		- nilai_cfpakar


	tbl

